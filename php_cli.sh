#!/bin/sh
export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
echo "Docker current working directory: '"$(pwd)"'"
docker run --rm \
  -it \
  -v $(pwd):/app \
  -v ~/.ssh:/root/.ssh \
  -w "/app" \
  --net=host \
  --tmpfs /run:rw,noexec,nosuid,size=65536k \
  --entrypoint="/usr/bin/php" \
  dockersymfony_php $@
